<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends BaseController {

    protected $template = "app";

    public function __construct() {
        parent::__construct();
        

    }

    public function index() {
        $this->data['halaman'] = "beranda";
        $this->render("index");
    }

    public function profile() {
        $this->data["changePassword"] = $this->input->post("change_password");
        if ($this->input->post("submit-button")) {
            if ($this->input->post("username") == null || $this->input->post("username") == "") {
                $this->data["errorUsernameMessage"] = "Username cannot be blank";
            }
            if ($this->input->post("email") == null || $this->input->post("email") == "") {
                $this->data["errorEmailMessage"] = "Email cannot be blank";
            }

            if ($this->input->post("change_password") != null && $this->input->post("change_password") == 1) {
                if ($this->input->post("password") == null || $this->input->post("password") == "") {
                    $this->data["errorPasswordMessage"] = "Password cannot be blank";
                }

                if ($this->input->post("confirm") == null || $this->input->post("confirm") == "") {
                    $this->data["errorConfirmMessage"] = "Password confirm cannot be blank";
                } else if ($this->input->post("confirm") != null && $this->input->post("confirm") != "" && $this->input->post("password") != $this->input->post("confirm")) {
                    $this->data["errorConfirmMessage"] = "Password confirm is not same as password";
                }
            }

            if (!isset($this->data["errorUsernameMessage"]) && !isset($this->data["errorEmailMessage"]) && !isset($this->data["errorPasswordMessage"]) && !isset($this->data["errorConfirmMessage"])) {
                $status = false;
                if ($this->input->post("change_password") != null && $this->input->post("change_password") == 1) {
                        $status = $this->db->query("UPDATE user SET email = ?, real_name = ?, password = SHA2(?, 256) WHERE id_user = ?", array($this->input->post("email"), $this->input->post("real_name"), $this->input->post("password"), $this->session->userdata("t_userId")));

                }

                // LOG USER
                $jenis_user = $this->globalFunction->jenis_user($this->session->userdata('t_idGroup'));
                $this->globalFunction->log_user($this->input->ip_address(),$this->session->userdata('t_userId'),$jenis_user,'Melakukan Perubahan Data Akun');

                if ($status) {
                    $this->data["successMessage"] = "Profile has been updated";
                } else {
                    $this->data["errorMessage"] = "Update failed, please try again.";
                }
            } else {
                $this->data["errorMessage"] = "Failed updating profile, please check below for errors.";
            }
        }

        $resultquery = $this->db->query("SELECT u.id_user, ug.nama_group, u.real_name, u.email
        FROM user u
        LEFT JOIN user_group_combo ugc ON ugc.id_user = u.id_user
        LEFT JOIN user_group ug ON ugc.id_group = ug.id_group
        WHERE u.id_user = ? AND ugc.id_group=?", array(intval($this->session->userdata("t_userId")), intval($this->session->userdata("t_idGroup"))));

        if ($resultquery->num_rows() == 0) {
            $resultset = $this->db->query("SELECT p.id_pegawai,p.url_photo, p.nama, ug.nama_group, p.email, p.id_group, p.is_active, p.nama_jabatan FROM pegawai p LEFT JOIN user_group ug ON ug.id_group = p.id_group WHERE p.id_pegawai = ? AND ug.id_group = ?", array(intval($this->session->userdata("t_userId")), intval($this->session->userdata("t_idGroup"))));
            $resultpersonil = $resultset->first_row();
            $this->data["detail"] = array(
                "id" => "1",
                "nama_group" => $resultpersonil->nama_jabatan,
                "username" => $resultpersonil->email,
                "email" => $resultpersonil->email,
                "photo" => $resultpersonil->url_photo,
            );
        } else {
            $result = $resultquery->first_row();
            $this->data["detail"] = array(
                "id" => "0",
                "nama_group" => $result->nama_group,
                "username" => $result->email,
                "email" => $result->email,
            );
        }

        $this->render("profile");
    }

    public function ganti_jabatan($id) {
        $resultsatker = $this->db->query("SELECT p.id_personil, p.email, p.real_name, p.password, p.telp, p.id_group, ug.nama_group, p.is_active, jk.id_jabatan, u.id_unit, jk.nama_jabatan FROM personil p LEFT JOIN jabatan_karyawan jk ON jk.id_jabatan = p.id_jabatan LEFT JOIN user_group ug ON p.id_group = ug.id_group LEFT JOIN unit u ON jk.id_unit = u.id_unit WHERE p.id_personil=? AND p.is_active = 1", $id);

        if (!empty($resultsatker)) {
            $query = $this->db->query("SELECT p.* FROM personil p WHERE p.id_personil=?", $this->session->userdata("t_userId"))->row_array();
            $query2 = $this->db->query("SELECT p.id_personil FROM personil p WHERE p.email=? AND p.real_name=? AND p.password=? AND p.telp=? AND p.is_active=1", array($query['email'], $query['real_name'], $query['password'], $query['telp']))->result_array();
            $id_personil = array();
            foreach ($query2 as $row) {
                $id_personil[] = $row['id_personil'];
            }
            if (in_array($id, $id_personil, true)) {
                //$this->unSetUserData();
                $rowsatker = $resultsatker->first_row();
                $this->session->set_userdata("t_userId", $rowsatker->id_personil);
                $this->session->set_userdata("t_username", $rowsatker->email);
                $this->session->set_userdata("t_realName", $rowsatker->real_name);
                $this->session->set_userdata("t_email", $rowsatker->email);
                $this->session->set_userdata("t_idGroup", $rowsatker->id_group);
                $this->session->set_userdata("t_idJabatan", $rowsatker->id_jabatan);
                $this->session->set_userdata("t_idUnit", $rowsatker->id_unit);
                $this->session->set_userdata("t_groupName", $rowsatker->nama_jabatan);
                $this->session->set_userdata("t_isActive", $rowsatker->is_active);
                $this->setUserData();
            } else {
                redirect();
            }
        }
        redirect();
    }

    public function forgot_password() {
        $this->template = "login";

        if ($this->input->post("reset-button")) {
            $result = $this->db->query("SELECT id_user FROM user WHERE email = ? AND is_active = '1'", array($this->input->post("email")));

            if ($result->num_rows() == 0) {
                $personil = $this->db->query("SELECT id_personil FROM personil WHERE email = ? AND is_active = '1'", array($this->input->post("email")));

                if ($personil->num_rows() == 0) {
                    $this->data["errorMessage"] = "Email tidak ditemukan/tidak valid.";
                } else {
                    $status = $this->db->query("INSERT INTO reset_password (email, reset_code, request_time) VALUES (?, SHA2(CAST(RAND() AS CHAR), 256), NOW())", array($this->input->post("email")));

                    if ($status) {
                        $result = $this->db->query("SELECT reset_code, request_time, email FROM reset_password WHERE email = ? ORDER BY request_time DESC LIMIT 1", array($this->input->post("email")));

                        if ($result->num_rows() > 0) {
                            $row = $result->first_row();

                            $requestTime = $this->tanggalIndo->konversi($row->request_time) . " pukul " . $this->tanggalIndo->jam($row->request_time) . " WIB";
                            $resetUrl = site_url("forgot_password_reset?token=" . $row->reset_code . "&email=" . $this->input->post("email"));

                            $emailContent = $this->load->view("index/forgot_password_email", array("resetUrl" => $resetUrl, "username" => $row->email, "requestTime" => $requestTime), true);

                            $status = $this->send_email_aktifasi($row->email, "Reset Password", $emailContent);

                            if ($status) {
                                $statusReset = $this->db->query("UPDATE reset_password SET sent_time = NOW() WHERE email = ? AND reset_time IS NULL", array($row->email));

                                if ($statusReset) {
                                    $this->data["successMessage"] = "Instruksi untuk reset password telah dikirim ke email anda.";
                                } else {
                                    $this->data["errorMessage"] = "Gagal memperbarui reset sent time, silahkan coba lagi.";
                                }
                            } else {
                                $this->data["errorMessage"] = "Gagal mengirim reset code ke email anda, silahkan coba lagi.";
                            }
                        } else {
                            $this->data["errorMessage"] = "Gagal membuat reset code, silahkan coba lagi.";
                        }
                    } else {
                        $this->data["errorMessage"] = "Fail to create reset code, please try again.";
                    }
                }
            } else {
                $status = $this->db->query("INSERT INTO reset_password (email, reset_code, request_time) VALUES (?, SHA2(CAST(RAND() AS CHAR), 256), NOW())", array($this->input->post("email")));

                if ($status) {
                    $result = $this->db->query("SELECT a.reset_code, a.request_time, a.email, b.email FROM reset_password a INNER JOIN user b ON a.email = b.email WHERE a.email = ? ORDER BY a.request_time DESC LIMIT 1", array($this->input->post("email")));

                    if ($result->num_rows() > 0) {
                        $row = $result->first_row();

                        $requestTime = $this->tanggalIndo->konversi($row->request_time) . " pukul " . $this->tanggalIndo->jam($row->request_time) . " WIB";
                        $resetUrl = site_url("forgot_password_reset?token=" . $row->reset_code . "&email=" . $this->input->post("email"));

                        $emailContent = $this->load->view("index/forgot_password_email", array("resetUrl" => $resetUrl, "username" => $row->email, "requestTime" => $requestTime), true);

                        $status = $this->send_email_aktifasi($row->email, "Reset Password", $emailContent);

                        if ($status) {
                            $statusReset = $this->db->query("UPDATE reset_password SET sent_time = NOW() WHERE email = ? AND reset_time IS NULL", array($row->email));

                            if ($statusReset) {
                                $this->data["successMessage"] = "Instruksi untuk reset password telah dikirim ke email anda.";
                            } else {
                                $this->data["errorMessage"] = "Gagal memperbarui reset sent time, silahkan coba lagi.";
                            }
                        } else {
                            $this->data["errorMessage"] = "Gagal mengirim reset code ke email anda, silahkan coba lagi.";
                        }
                    } else {
                        $this->data["errorMessage"] = "Gagal membuat reset code, silahkan coba lagi.";
                    }
                } else {
                    $this->data["errorMessage"] = "Fail to create reset code, please try again.";
                }
            }
        }

        $this->render("forgot_password");
    }

    public function forgot_password_reset() {
        $this->template = "login";

        $this->data["token"] = $this->input->get("token");

        if ($this->input->post("reset-button") != null) {
            if ($this->input->post("email") == "") {
                $this->data["errorEmailMessage"] = "Email harus diisi.";
            } else if ($this->input->post("password") == "") {
                $this->data["errorPasswordMessage"] = "Password harus diisi.";
                $this->data["pass1_temp"] = $this->input->post("password");
                $this->data["pass2_temp"] = $this->input->post("confirm");
            } else if ($this->input->post("password") != $this->input->post("confirm")) {
                $this->data["errorConfirmPasswordMessage"] = "Konfirmasi Password tidak sesuai dengan password baru.";
                $this->data["pass1_temp"] = $this->input->post("password");
                $this->data["pass2_temp"] = $this->input->post("confirm");
            } else {
                $check = $this->db->query("SELECT email FROM user WHERE email = ? AND is_active = '1'", array($this->input->post("email")));

                if ($check->num_rows() == 0) {
                    $personil = $this->db->query("SELECT email FROM personil WHERE email = ? AND is_active = '1'", array($this->input->post("email")));

                    if ($personil->num_rows() == 0) {
                        $table = null;
                    } else {
                        $table = "personil";
                    }
                } else {
                    $table = "user";
                }

                if ($table != null) {
                    $result = $this->db->query("SELECT email FROM reset_password WHERE reset_code = ? AND email = ? ORDER BY request_time DESC LIMIT 1", array($this->input->get("token"), $this->input->post("email")));

                    if ($result->num_rows() == 0) {
                        $this->data["errorEmailMessage"] = "Gagal mem-verifikasi email dan token, harap pastikan email yang anda masukkan sudah benar.";
                    } else {
                        $statusReset = $this->db->query("UPDATE reset_password SET reset_time = NOW() WHERE email = ? AND reset_time IS NULL", array($this->input->post("email")));

                        if ($statusReset) {
                            $statusChangePassword = $this->db->query("UPDATE " . $table . " SET password = SHA2(?, 256) WHERE email = ?", array($this->input->post("password"), $this->input->post("email")));

                            if ($statusChangePassword) {
                                $this->data["successMessage"] = "Password berhasil diperbarui, silahkan kembali untuk login menggunakan password baru anda.";
                                redirect("?forgot_password=true");
                            } else {
                                $this->data["errorEmailMessage"] = "Gagal me-reset password, silahkan coba reset lagi <a href='" . site_url("forgot_password") . "'>disini</a> untuk mendapat link reset password baru.";
                            }
                        } else {
                            $this->data["errorEmailMessage"] = "Gagal flag reset password, silahkan coba lagi.";
                            $this->data["pass1_temp"] = $this->input->post("password");
                            $this->data["pass2_temp"] = $this->input->post("confirm");
                        }
                    }
                } else {
                    $this->data["errorEmailMessage"] = "Maaf, email anda tidak dapat ditemukan pada database. Mungkin telah dihapus oleh Admin.";
                }
            }

            $this->render("forgot_password_reset");
        } else {
            if ($this->input->get("token") == null || $this->input->get("token") == "") {
                exit("Token kosong.");
            } else {
                $result = $this->db->query("SELECT email FROM reset_password WHERE reset_code = ? AND reset_time IS NULL ORDER BY request_time DESC LIMIT 1", array($this->input->get("token")));

                if ($result->num_rows() == 0) {
                    exit("Token tidak valid");
                } else {
                    $this->render("forgot_password_reset");
                }
            }
        }
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function send_email_aktifasi($recipient, $subyek, $message) {
        date_default_timezone_set('Asia/Jakarta');

        $data = array('recipient' => $recipient, 'subyek' => $subyek, 'message' => $message);
        $options = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($this->email_handler->url(), false, $context);

        if (strpos($result, "email sent!") == true) {
            return true;
        } else {
            return false;
        }

    }

    public function logout() {
        // LOG USER
        $jenis_user = $this->globalFunction->jenis_user($this->session->userdata('t_idGroup'));
        $this->globalFunction->log_user($this->input->ip_address(),$this->session->userdata('t_userId'),$jenis_user,'Logout Aplikasi Web');

        $this->unSetUserData();
        redirect("?logout=true");
    }

    public function set_admin() {
        $this->session->set_userdata('triggered', '1');
        $this->session->set_userdata("t_groupName", "JASKO");
        redirect("index");
    }

}
