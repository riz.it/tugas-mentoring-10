<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Tour extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';


    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/Api_tour');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function list_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);

        if ($data['kategori']!='' || $data['kategori']!=NULL) {

            $result = $this->Api_tour->getTour($data['kategori']);

            if ($result['status']!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);

            }else {

                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function detail_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);

        if ($data['id_tour']!='' || $data['id_tour']!=NULL) {

            $result = $this->Api_tour->detailTour(str_replace("-", "", $data['id_tour']));

            if ($result['status']!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);

            }else {

                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }



    public function konfirmasi_post()
    {

        $headers = $this->input->request_headers();

        $headers = array_change_key_case($headers,CASE_LOWER);

        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

            $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= FALSE && property_exists($decoded_token, "id_user")) {

                $data =json_decode(trim(file_get_contents('php://input')), true);

                if (is_array($data)) {
                    $result = $this->Api_tour->konfirmasi_pesanan($data);
                    if ($result) {
                      $this->response([
                                'status'=>$this->ok,
                                'message'=>'Data pemesanan tour berhasil diproses, harap cek email anda untuk mengunduh invoice yang dikirimkan',
                                'data'=>'1'],
                                REST_Controller::HTTP_OK);
                    }else {
                      $this->response([
                                'status'=>$this->errot,
                                'message'=>'Data pemesanan tour berhasil gagal, harap cek email anda untuk mengunduh invoice yang dikirimkan',
                                'data'=>'1'],
                                REST_Controller::HTTP_OK);
                    }
                }else {
                    $this->response(['status'=>$this->error,'message'=>'Entri data failed','data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
                }

            }else {

                $this->response([
                    'status' => $this->bad,
                    'message' => 'Token tidak ditemukan.',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

        }else {

            $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function getPesanan_post()
    {
        $headers = $this->input->request_headers();

        $headers = array_change_key_case($headers,CASE_LOWER);

        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {
            $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= FALSE && property_exists($decoded_token, "id_user")) {

                $data =json_decode(trim(file_get_contents('php://input')), true);
                if (!empty($data)) {
                    $result = $this->Api_tour->pemesanan_customer($data['id_customer']);
                    if (!empty($result)) {
                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>'Data customer tidak ditemukan',
                            'data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
                    }
                }else {
                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data customer tidak terdaftar',
                        'data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
                }

            }else {

                $this->response([
                    'status' => $this->bad,
                    'message' => 'Token tidak ditemukan.',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

        }else {

            $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }


    }

    public function detailPesanan_post()
    {
        $headers = $this->input->request_headers();

        $headers = array_change_key_case($headers,CASE_LOWER);

        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

            $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= FALSE && property_exists($decoded_token, "id_user")) {

                $data =json_decode(trim(file_get_contents('php://input')), true);

                  if (!empty($data)) {

                    $result = $this->Api_tour->detail_pesanan($data['id_pemesanan']);

                    if (!empty($result)) {

                            $this->response([
                                'status'=>$this->ok,
                                'message'=>'Data pemesanan',
                                'data'=>$result['data']], REST_Controller::HTTP_OK);

                        }else {

                            $this->response([
                                'status'=>$this->error,
                                'message'=>'Data pemesanan tidak ditemukan',
                                'data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
                        }

                  }else {

                        $this->response([
                            'status'=>$this->error,
                            'message'=>'Data pesanan tidak terdaftar',
                            'data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);

                  }

            }else {

                $this->response([
                    'status' => $this->bad,
                    'message' => 'Token tidak ditemukan.',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

        }else {

            $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }


    }

    public function filter_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);

        if ($data['jenis']!='') {

            $result = $this->Api_tour->getFilter($data['jenis']);

            if ($result['status']!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else {
            $this->response([
                'status'=>$this->bad,
                'message'=>'Parameter tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function durasi_get()
    {

        $result = $this->Api_tour->getDurasi();

        if ($result['status']!='failed') {

            $this->response([
                'status'=>$this->ok,
                'message'=>$result['message'],
                'data'=>$result['data']], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'status'=>$this->error,
                'message'=>$result['message'],
                'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function editPeserta_post()
    {
        $headers = $this->input->request_headers();

        $headers = array_change_key_case($headers,CASE_LOWER);

        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

            $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= FALSE && property_exists($decoded_token, "id_user")) {

                $data =json_decode(trim(file_get_contents('php://input')), true);

                  if (!empty($data)) {

                    $result = $this->Api_tour->editPeserta($data['id_peserta']);

                    if (!empty($result)) {

                            $this->response([
                                'status'=>$this->ok,
                                'message'=>'Data peserta tour ditemukan',
                                'data'=>$result['data']], REST_Controller::HTTP_OK);

                        }else {

                            $this->response([
                                'status'=>$this->error,
                                'message'=>'Data peserta tour tidak ditemukan',
                                'data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);
                        }

                  }else {

                        $this->response([
                            'status'=>$this->error,
                            'message'=>'Data peserta tour tidak terdaftar',
                            'data'=>'0'], REST_Controller::HTTP_BAD_REQUEST);

                  }

            }else {

                $this->response([
                    'status' => $this->bad,
                    'message' => 'Token tidak ditemukan.',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

        }else {

            $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function updatePeserta_post()
    {
        $headers = $this->input->request_headers();

        $headers = array_change_key_case($headers,CASE_LOWER);

        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

            $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= FALSE && property_exists($decoded_token, "id_user")) {

                $data['nama_lengkap'] = $this->input->post('nama_lengkap');
                $data['jenis_identitas'] = $this->input->post('jenis_identitas');
                $data['nomor_identitas'] = $this->input->post('nomor_identitas');
                $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
                $data['tempat_lahir'] = $this->input->post('tempat_lahir');
                $data['tanggal_lahir'] = $this->input->post('tanggal_lahir');
                $data['email'] = $this->input->post('email');
                $data['nomor_hp'] = $this->input->post('nomor_hp');
                $data['status_kawin'] = $this->input->post('status_kawin');
                $data['pendidikan'] = $this->input->post('pendidikan');
                $data['pekerjaan'] = $this->input->post('pekerjaan');
                $data['id_provinsi'] = $this->input->post('id_provinsi');
                $data['id_kota'] = $this->input->post('id_kota');
                $data['id_kecamatan'] = $this->input->post('id_kecamatan');
                $data['kode_pos'] = $this->input->post('kode_pos');
                $data['alamat_ktp'] = $this->input->post('alamat_ktp');
                $data['alamat_domisili'] = $this->input->post('alamat_domisili');
                $data['nama_ayah'] = $this->input->post('nama_ayah');
                $data['nama_ibu'] = $this->input->post('nama_ibu');
                $data['nomor_paspor'] = $this->input->post('nomor_paspor');
                $data['nama_paspor'] = $this->input->post('nama_paspor');
                $data['tanggal_dikeluarkan'] = $this->input->post('tanggal_dikeluarkan');
                $data['tanggal_berlaku'] = $this->input->post('tanggal_berlaku');
                $data['kantor_penerbit'] = $this->input->post('kantor_penerbit');
                $data['last_modified'] = date('Y-m-d h:i:s');

                $data_lama = $this->Api_tour->editPeserta($this->input->post('id_peserta'));

                if (!empty($this->input->post('ubah_foto')) && $this->input->post('ubah_foto')=='1') {

                    if ($data_lama['data']->foto_profil!='' || $data_lama['data']->foto_profil!=NULL) {
                        if (file_exists('./'.$data_lama['data']->foto_profil)) {
                            unlink('./'.$data_lama['data']->foto_profil);
                        }
                    }

                    if (isset($_FILES['foto_profil']['name'])) {
                        list($width, $height) = getimagesize($_FILES['foto_profil']['tmp_name']);
                        $config['upload_path'] = 'files/tour_peserta/'; //path folder file upload
                        $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                        $config['max_size'] = '2000';
                        $config['file_name'] = "profile_" . date('ymdhis'); //enkripsi file name upload
                        $this->load->library('upload');
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('foto_profil')) {
                            $file_foto = $this->upload->data();
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './files/tour_peserta/' . $file_foto['file_name'];
                            $config['create_thumb'] = FALSE;
                            $config['maintain_ratio'] = TRUE;
                            $config['quality'] = '50%';
                            $config['width'] = round($width / 2);
                            $config['height'] = round($height / 2);
                            $config['new_image'] = './files/tour_peserta/' . $file_foto['file_name'];
                            $this->load->library('image_lib');
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $nama_foto = 'files/tour_peserta/' . $file_foto['file_name'];
                            $data['foto_profil'] = $nama_foto;
                        }
                    }
                }else {
                    $data['foto_profil'] = $data_lama['data']->foto_profil;
                }



                if (!empty($this->input->post('ubah_paspor')) && $this->input->post('ubah_paspor')=='1') {

                    if ($data_lama['data']->scan1_paspor!='' || $data_lama['data']->scan1_paspor!=NULL) {
                        if (file_exists('./'.$data_lama['data']->scan1_paspor)) {
                            unlink('./'.$data_lama['data']->scan1_paspor);
                        }
                    }

                    if ($data_lama['data']->scan2_paspor!='' || $data_lama['data']->scan2_paspor!=NULL) {
                        if (file_exists('./'.$data_lama['data']->scan2_paspor)) {
                            unlink('./'.$data_lama['data']->scan2_paspor);
                        }
                    }

                    if (isset($_FILES['scan1_paspor']['name'])) {
                        list($width, $height) = getimagesize($_FILES['scan1_paspor']['tmp_name']);
                        $config['upload_path'] = 'files/tour_paspor/'; //path folder file upload
                        $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                        $config['max_size'] = '2000';
                        $config['file_name'] = "paspor1_" . date('ymdhis'); //enkripsi file name upload
                        $this->load->library('upload');
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('scan1_paspor')) {
                            $file_foto = $this->upload->data();
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './files/tour_paspor/' . $file_foto['file_name'];
                            $config['create_thumb'] = FALSE;
                            $config['maintain_ratio'] = TRUE;
                            $config['quality'] = '50%';
                            $config['width'] = round($width / 2);
                            $config['height'] = round($height / 2);
                            $config['new_image'] = './files/tour_paspor/' . $file_foto['file_name'];
                            $this->load->library('image_lib');
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $nama_foto = 'files/tour_paspor/' . $file_foto['file_name'];
                            $data['scan1_paspor'] = $nama_foto;
                        }
                    }

                    if (isset($_FILES['scan2_paspor']['name'])) {
                        list($width, $height) = getimagesize($_FILES['scan2_paspor']['tmp_name']);
                        $config['upload_path'] = 'files/tour_paspor/'; //path folder file upload
                        $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                        $config['max_size'] = '2000';
                        $config['file_name'] = "paspor2_" . date('ymdhis'); //enkripsi file name upload
                        $this->load->library('upload');
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('scan2_paspor')) {
                            $file_foto = $this->upload->data();
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './files/tour_paspor/' . $file_foto['file_name'];
                            $config['create_thumb'] = FALSE;
                            $config['maintain_ratio'] = TRUE;
                            $config['quality'] = '50%';
                            $config['width'] = round($width / 2);
                            $config['height'] = round($height / 2);
                            $config['new_image'] = './files/tour_paspor/' . $file_foto['file_name'];
                            $this->load->library('image_lib');
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $nama_foto = 'files/tour_paspor/' . $file_foto['file_name'];
                            $data['scan2_paspor'] = $nama_foto;
                        }
                    }
                }else {
                    $data['scan1_paspor'] = $data_lama['data']->scan1_paspor;
                    $data['scan2_paspor'] = $data_lama['data']->scan2_paspor;
                }

                $result=$this->Api_tour->updatePeserta(str_replace("-", "", $this->input->post('id_peserta')), $data);
                if ($result['status']!='failed') {
                    $this->response([
                                'status'=>$this->ok,
                                'message'=>$result['message'],
                                'data'=>'1'], REST_Controller::HTTP_OK);
                }else {
                    $this->response([
                                'status'=>$this->error,
                                'message'=>$result['message'],
                                'data'=>'0'], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }



            }else {

                $this->response([
                    'status' => $this->bad,
                    'message' => 'Token tidak ditemukan.',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

        }else {

            $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }
}
