<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "member";

    public function __construct()
    {
        parent::__construct();
        
        $this->cek_hak_akses();
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Tim Devisi';
        $this->data['condition'] = '<div class="breadcrumb-item active">Devisi</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('members');
        $crud->setRelation('id_devisi','ref_devisi','nama_devisi');
        $crud->setRelation('id_provinsi','ref_provinsi','name');
        $crud->setRelation('id_kabupaten','ref_kabkota','name');

        $crud->columns(['id_devisi','nama','nomor_hp','email','id_provinsi','id_kabupaten','alamat']);

        $crud->fields(['id_devisi','nama','nomor_hp','email','id_provinsi','id_kabupaten','alamat','id_user']);
        $crud->requiredFields(['id_devisi','nama','nomor_hp','email']);
        $crud->defaultOrdering('nama', 'asc');
        $display = [
            'id_devisi'=>'Devisi',
            'nama'=>'Nama Lengkap',
            'nomor_hp'=>'Nomor HP',
            'email'=>'Email',
            'id_provinsi'=>'Provinsi',
            'id_kabupaten'=>'Kabupaten',
            'alamat'=>'Alamat'];
        
        $crud->displayAs($display);
        $crud->fieldType('id_user','hidden');

        $crud->callbackBeforeInsert([$this,'_callBeforeInsert']);

        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output,'index');
    }

    public function _callBeforeInsert($params)
    {
        $params->data['id'] = getUUID();
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        return $params;
    }

    function _setOutput($output = null, $view=null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('devisi/index/'.$view, $x);
        $this->layout->publish();
    }

}
