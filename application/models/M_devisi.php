<?php defined('BASEPATH')OR exit('no access allowed');
/**
  * summary
  */
 class M_devisi extends MY_Model
 {
     /**
      * summary
      */
    protected $_table_name = "ref_devisi";
    protected $_order_by ="id";
    protected $_order_by_type ="ASC";
    protected $_primary_key = "id";


     public function __construct()
     {
     	parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
     }

     public function cekKode($kode)
     {
         $return['success'] = false;
         $return['message'] = '';
         
         $get = $this->db->query("SELECT * FROM ref_devisi where kode_devisi=?",array($kode));
         if ($get->num_rows()!=0) {
             $return['success'] = false;
             $return['message'] = 'Kode sudah digunakan';
            } else {
             $return['success'] = true;
             $return['message'] = 'Kode bisa digunakan';
         }
         return $return;
         
     }

 }
?>