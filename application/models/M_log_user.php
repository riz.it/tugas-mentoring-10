<?php

class M_log_user extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function set_date() {
        $periode = $this->input->get("periode");

        if ($periode) {
            $tgl = urldecode($periode);

            $this->f_awal = date("Y-m-d", strtotime(substr($tgl, 0, 10)));
            $this->f_akhir = date("Y-m-d", strtotime(substr($tgl, 13, 23)));
        } else {
            $this->f_awal = date('Y-m-d', strtotime('-3 days'));
            $this->f_akhir = date('Y-m-d');
        }
        $awal = date("m/d/Y", strtotime($this->f_awal));
        $akhir = date('m/d/Y', strtotime($this->f_akhir));

        $array = array(
            'tgl_awal' => $this->f_awal,
            'tgl_akhir' => $this->f_akhir,
            'tglawal' => $awal,
            'tglakhir' => $akhir
        );
        return $array;
    }

    function get_filter_data_user() {
        $query = $this->db->query("SELECT id_group, nama_group  FROM user_group WHERE is_active='1'")->result_array();
        return $query;
    }

    function get_kabupaten() {
        $query = $this->db->query("SELECT id_kabupaten, nama_kabupaten  FROM kabupaten WHERE is_active='1'")->result_array();
        return $query;
    }

    function get_kecamatan() {
        $where = '';
        $id_kabupaten = $this->input->get('kabupaten');

        if (!empty($id_kabupaten)) {
            if ($id_kabupaten == 'all') {
                $where = '';
            } else {
                $where .= " AND id_kabupaten ='$id_kabupaten'";
            }
        }
        $query = $this->db->query("SELECT id_kecamatan, nama_kecamatan  FROM kecamatan WHERE is_active='1' $where")->result_array();
        return $query;
    }

    function get_all_data($jenis_return, $start = null, $limit = null) {

        if ($jenis_return == 'jumlah')
            $kolom = 'COUNT(lu.id_log) as jml';
        else {
            $kolom = " * ";
        }

        $tabel = "FROM log_user lu
                        JOIN user u ON u.id_user=lu.id_user";
        $id_kabupaten = $this->input->get('kabupaten');
        $id_kecamatan = $this->input->get('kecamatan');
        if (!empty($id_kabupaten)) {
            $tabel = " FROM log_user lu
                        JOIN user u ON u.id_user=lu.id_user
                        JOIN kabupaten k ON k.id_user=lu.id_user WHERE k.is_active='1' ";
        }
        if (!empty($id_kecamatan)) {
            $tabel = " FROM log_user lu
                       JOIN user u ON u.id_user=lu.id_user
                       JOIN kecamatan k ON k.id_user=lu.id_user WHERE k.is_active='1' ";
        }
        $orderby = " ORDER BY lu.activity_time DESC";

        if (!empty($this->input->get())) {
            $where = $this->whereHandler();
            $query = $this->db->query("SELECT $kolom $tabel $where $orderby");
        } else {
            if (isset($limit)) {
                $query = $this->db->query("SELECT $kolom $tabel $orderby LIMIT $start, $limit");
            } else {
                $query = $this->db->query("SELECT $kolom $tabel $orderby");
            }
        }

        if ($jenis_return == 'jumlah')
            return $query->first_row()->jml;
        else {
            //  var_dump($this->db->last_query()); exit();
            return $query->result_array();
        }
    }

    function whereHandler() {
        $where = '';
        $id_kabupaten = $this->input->get('kabupaten');
        $id_kecamatan = $this->input->get('kecamatan');

        if (!empty($id_kabupaten)) {
            if ($id_kabupaten == 'all') {
                $where = '';
            } else {
                $where .= " AND k.id_kabupaten ='$id_kabupaten'";
            }
        }
        if (!empty($id_kecamatan)) {
            if ($id_kecamatan == 'all') {
                $where = '';
            } else {
                $where = " AND k.id_kecamatan = '$id_kecamatan'";
            }
        }
        return $where;
    }

    public function get_file_bukti($id) {
        $query = $this->db->query("SELECT * FROM monev_bukti WHERE id_monev=?", $id)->result_array();
        return $query;
    }

    public function get_data_lokasi($id_monev) {
        $parameters = array();
        $parameters[] = $id_monev;

        $query = $this->db->query("SELECT latitude, longitude FROM monev WHERE id_monev = ?", $parameters)->row_array();
        return $query;
    }

}
