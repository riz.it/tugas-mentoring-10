<?php defined('BASEPATH')OR exit('no access allowed');
/**
  * summary
  */
 class M_product extends MY_Model
 {
     /**
      * summary
      */
    protected $_table_name = "products";
    protected $_order_by ="id";
    protected $_order_by_type ="ASC";
    protected $_primary_key = "id";

    public function getAllProduct()
    {
      $this->db->select('products.*, categories.category');
      $this->db->from('products');
      $this->db->join('categories', 'categories.id = products.category_id');
      return $this->db->get()->result_array();
    }

    public function getByCategory($param)
    {
      $this->db->select('products.*');
      $this->db->from('products');
      $this->db->where('products.category_id', $param);
      return $this->db->get()->result_array();
    }

    public function getByUUID($param)
    {
      $this->db->select('products.*, categories.name as category_name');
      $this->db->from('products');
      $this->db->join('categories', 'categories.uuid = products.category_id');
      $this->db->where('products.uuid', $param);
      return $this->db->get()->row_array();
    }

 }
