<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_auth extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function login($username, $password)
    {
        $return = null;
        $dt_user = null;
        $det_user = null;
        if ($username=='' && $password=='') {
            return ['status'=>'failed','message'=>'username/password not allowed nulled','data'=>'0'];
        }else {
            $get = $this->db->query("SELECT user.id_user, user.email, user.username, user.real_name,user_group_combo.id_group FROM user right join user_group_combo on user_group_combo.id_user=user.id_user  where ((username IS NOT NULL AND username=?) OR (email IS NOT NULL AND email=?)) and password=SHA2(?,256) AND user.is_active=1", array($username, $username, $password));
            if ($get->num_rows()>0) {
                $r = $get->row();

                $dt_user['id_user'] = $r->id_user;
                $dt_user['email'] = $r->email;
                $dt_user['username'] = $r->username;
                $dt_user['real_name'] = $r->real_name;
                $dt_user['id_group'] = $r->id_group;

                if ($dt_user['id_group']=='9') {

                    $detail = $this->db->query("SELECT cs.id_customer,cs.nik,cs.nama, cs.nama_ayah,cs.id_cabang,cs.tempat_lahir,
                        cs.tanggal_lahir, cs.jenis_kelamin, cs.status, cs.nomor_hp, cs.email,cs.pendidikan, cs.pekerjaan,
                        cs.penghasilan,cs.id_provinsi, cs.id_kabupaten, cs.id_kecamatan,
                        prov.nama_propinsi, kab.nama_kab_kota,kec.nama_kecamatan, cs.alamat, cs.url_ktp, cs.url_foto, cs.is_tab_umrah,cs.is_umrah, cs.saldo, cb.nama_cabang from customer cs
       left JOIN cabang cb ON REPLACE(cb.id_cabang,'-','')=REPLACE(cs.id_cabang,'-','')
      left join ref_provinsi prov on prov.kode_propinsi=cs.id_provinsi
      left join ref_kab_kota kab ON  kab.kode_kab_kota=cs.id_kabupaten
      left join ref_kecamatan kec on kec.kode_kecamatan=cs.id_kecamatan
      WHERE cs.fk_id_user=?",array($dt_user['id_user']))->row();

                    $det_user['id_role'] = $dt_user['id_group'];
                    $det_user['id_customer'] = $detail->id_customer;
                    $det_user['nik'] = $detail->nik;
                    $det_user['nik'] = $detail->nik;
                    $det_user['nama'] = $detail->nama;
                    $det_user['nama_ayah'] = $detail->nama_ayah;
                    $det_user['id_cabang'] = $detail->id_cabang;
                    $det_user['tempat_lahir'] = $detail->tempat_lahir;
                    $det_user['tanggal_lahir'] = $detail->tanggal_lahir;
                    $det_user['jenis_kelamin'] = $detail->jenis_kelamin;
                    $det_user['status'] = $detail->status;
                    $det_user['nomor_hp'] = $detail->nomor_hp;
                    $det_user['email'] = $detail->email;
                    $det_user['type'] = "";
                    $det_user['pendidikan'] = $detail->pendidikan;
                    $det_user['pekerjaan'] = $detail->pekerjaan;
                    $det_user['penghasilan'] = $detail->penghasilan;
                    $det_user['id_provinsi'] = $detail->id_provinsi;
                    $det_user['id_kabupaten'] = $detail->id_kabupaten;
                    $det_user['id_kecamatan'] = $detail->id_kecamatan;
                    $det_user['nama_propinsi'] = $detail->nama_propinsi;
                    $det_user['nama_kab_kota'] = $detail->nama_kab_kota;
                    $det_user['nama_kecamatan'] = $detail->nama_kecamatan;
                    $det_user['alamat'] = $detail->alamat;
                    $det_user['url_ktp'] = $detail->url_ktp;
                    $det_user['url_foto'] = $detail->url_foto;
                    $det_user['is_tab_umrah'] = $detail->is_tab_umrah;
                    $det_user['is_umrah'] = $detail->is_umrah;
                    $det_user['saldo'] = $detail->saldo;
                    $det_user['kantor_cabang'] = $detail->nama_cabang;

                }else {
                    return ['status'=>'failed','message'=>'user not allowed','data'=>'0'];
                }

                $return = $dt_user;
                $return['data_user'] = $det_user;
                return ['status'=>'success','message'=>'data customer ditemukan', 'data'=>$return];
            }else {
                return ['status'=>'failed','message'=>'data customer tidak ditemukan','data'=>'0'];
            }

        }

    }

    function check_user($username) {
        //cek username
        $get = $this->db->query("SELECT id_user, id_group FROM user WHERE ((username IS NOT NULL AND LOWER(username) = ?) OR (email IS NOT NULL AND LOWER(email) = ?))", array($username, $username));
        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Refresh token gagal. User tidak ditemukan."];
        }

        //cek status
        $get = $this->db->query("SELECT id_user, id_group, is_active FROM user WHERE ((username IS NOT NULL AND LOWER(username) = ?) OR (email IS NOT NULL AND LOWER(email) = ?))", array($username, $username))->row_array();
        if ($get['is_active'] == '0') {
            return ["status" => "failed", "message" => "Refresh token gagal. User telah dinonaktifkan."];
        } else if (!(in_array($get['id_group'], $this->allowed_user_group))) {
            return ["status" => "failed", "message" => "Login gagal. User role tidak diperbolehkan."];
        } else if ($get['id_group'] == null || $get['id_group'] == '0') {
            return ["status" => "failed", "message" => "Login gagal. User role tidak ditemukan."];
        }

        $id_user = $get['id_user'];
        $id_group = $get['id_group'];
        return ["status" => "ok", "data" => ['id_user' => $id_user, 'id_group' => $id_group]];
    }

}
