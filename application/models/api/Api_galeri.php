<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_galeri extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function data_galeri($limit="") {
        $this->db->select("*");
        $this->db->from("galeri_foto");
        $this->db->order_by("galeri_foto.created_at", 'DESC');
        if ($limit !="" || $limit!=null) {
            $this->db->limit($limit);
        }
        $get = $this->db->get();

        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan."];
        }
        $i = 0;
        foreach ($get->result() as $key => $r) {
            $result[$i]['id_foto'] = $r->id_foto;
            $result[$i]['judul_foto'] = $r->judul_foto;
            $result[$i]['caption_foto'] = $r->caption_foto;
            $result[$i]['deskripsi_foto'] = $r->deskripsi_foto;
            $result[$i]['link_foto'] = $r->link_foto;
            $i++;
        }

        // serve
        return ["status" => "ok", "data" => $result];
    }
}
