<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?php echo isset($nama_menu)?$nama_menu:''; ?></h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active">
              </div>
              <?php echo isset($condition)?$condition:''; ?>
            </div>
          </div>
          <div class="section-body">
            <div class="card card-info">
                <div class="card-body">
                    <h3>Selamat datang, <?php echo $this->session->userdata('t_realName') ?></h3>
                </div>
                <button class="btn btn-success" id="btn-check" type="button"><i class="fas fa-check"></i></button>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('template/template_scripts') ?>
<script>
  var site_url = '<?php echo site_url() ?>';
  $(function () {
    $('#btn-check').click(function (e) {
      e.preventDefault();
      $.ajax({
        type : 'ajax',
        method : 'post',
        url : site_url+'/index/cekAPI',
        data : {nik : '3213231207800006'},
        dataType : 'json',
        success:function (response) {
          console.log(response);
        },
        error: function (xmlresponse) {
          console.log('errpr '+xmlresponse);
        }
      })
    })
  })
</script>