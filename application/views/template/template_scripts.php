<!-- General JS Scripts -->
<script src="<?php echo base_url('public/modules/js/jquery-3.3.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/modules/js/google.js') ?>"></script>
<script src="<?php echo base_url('public/modules/js/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/js/tooltip.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/js/jquery.nicescroll.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/js/moment-with-locales.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/select2/select2-dropdownPosition.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('public/js/stisla.js') ?>"></script>
<script src="<?php echo base_url('public/js/loadingoverlay.js') ?>"></script>
<script src="<?php echo base_url() ?>public/js/scripts.js"></script>
<script src="<?php echo base_url() ?>public/js/custom.js"></script>
<script src="<?php echo base_url('public/modules/sweetalert2/sweetalert2.js') ?>"></script>
<!-- JS Libraies -->
<!-- datatables -->
<script src="<?php echo base_url('public/modules/datatables/datatables.min.js') ?>"></script>
<script src="<?php echo base_url('public/modules/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<!-- Template JS File -->
<script src="<?php echo base_url('public/modules/chosen/chosen.jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/summernote/summernote.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/summernote/summernote-lite.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/summernote/summernote-bs4.min.js'); ?>"></script>
<script src="<?php echo base_url('public/modules/') ?>jquery-mask/dist/jquery.mask.js" type="text/javascript"></script>
<script src="<?php echo base_url('public/modules/bootstrap3-editable') ?>/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<!-- iziToast -->
<script src="<?php echo base_url('public/modules/iziToast/dist/js') ?>/iziToast.min.js"></script>
<!-- jquery-validate -->
<script src="<?php echo base_url('public/modules/jquery-validation/jquery.validate.min.js') ?>"></script>

<!-- Page Specific JS File -->
<script type="text/javascript">
  $(function() {
    $('.uang').mask('000.000.000.000.000', {
      reverse: true
    });

    $('.tanggal').datepicker({
      format: 'yyyy-mm-dd',
      language: 'id',
      autoclose: true
    });
  })

  function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

      return false;
    return true;
  }

  function show_loading() {
    document.getElementById("spinner-front").classList.add("show");
    document.getElementById("spinner-back").classList.add("show");
  }

  function hide_loading() {
    document.getElementById("spinner-front").classList.remove("show");
    document.getElementById("spinner-back").classList.remove("show");
  }

  function readURL(input, type) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        if (type == "simpan") {
          $('#tampil_sim').attr('src', e.target.result);

        } else if (type == "profil") {
          $('#tampil_sim_profil').attr('src', e.target.result);

        }else if (type == "edit") {
          $('#tampil_up').attr('src', e.target.result);

        } else if (type = "scan1") {
          $('#tampil_scan1').attr('src', e.target.result);

        } else if (type = "scan2") {
          $('#tampil_scan2').attr('src', e.target.result);

        }
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
</script>